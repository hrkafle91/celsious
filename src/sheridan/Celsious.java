package sheridan;

public class Celsious {

	public static void main(String[] args) {
		System.out.println(convertFromFahrenheit(8));
	}
	
	public static int convertFromFahrenheit( int fahrenheit ) {
		int celsious = (int) Math.ceil(((fahrenheit - 32) * 5/9));
		
		return celsious;
	}
}
