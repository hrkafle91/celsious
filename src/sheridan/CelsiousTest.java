package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CelsiousTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testConvertFromFahrenheit() {
		int celsious = Celsious.convertFromFahrenheit(32);
		assertTrue("Invalid value for celsious", celsious == 0);
	}
	@Test
	public void testIsPalindromeNegative() {
		int celsious = Celsious.convertFromFahrenheit(32);
		assertFalse("Invalid value for celsious", celsious == 10);
	}
	@Test
	public void testIsPalindromeBoundaryIn() {
		int celsious = Celsious.convertFromFahrenheit(7);
		assertTrue("Invalid value for celsious", celsious == -13);
	}
	@Test
	public void testIsPalindromeBoundaryOut() {
		int celsious = Celsious.convertFromFahrenheit(8);
		assertFalse("Invalid value for celsious", celsious > -13);
	}

}
